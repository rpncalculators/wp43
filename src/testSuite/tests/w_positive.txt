;*************************************************************
;*************************************************************
;**                                                         **
;**                        Lambert W                        **
;**                                                         **
;*************************************************************
;*************************************************************
In: FL_SPCRES=0 FL_CPXRES=0 SD=0 RM=0 IM=2compl SS=4 WS=64
Func: fnWpositive



;=======================================
; W0(Long Integer) --> Real
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=LonI:"1"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=LonI:"1" RX=Real:"0.5671432904097838729999686622103555"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=LonI:"0"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=LonI:"0" RX=Real:"0"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=LonI:"-1"
Out: EC=1 FL_CPXRES=0 FL_ASLIFT=0 RX=LonI:"-1"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=1 RX=LonI:"-1"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=LonI:"-1" RX=Cplx:"-.3181315052047641353126542515876645 i 1.337235701430689408901162143193711"



;=======================================
; W0(Time) --> Error24
;=======================================



;=======================================
; W0(Date) --> Error24
;=======================================



;=======================================
; W0(String) --> Error24
;=======================================
In:  FL_ASLIFT=0 RX=Stri:"String test"
Out: EC=24 FL_ASLIFT=0 RX=Stri:"String test"



;=======================================
; W0(Real Matrix) --> Real Matrix
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=ReMa:"M2,3[0,1,2,3,4,5]"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=ReMa:"M2,3[0,1,2,3,4,5]" RX=ReMa:"M2,3[0,0.567143290409783872999968662210355549754,0.852605502013725491346472414695317466898,1.04990889496403995998869707055289790459,1.20216787319704293921207416549515344750,1.32672466524220022363509929775807966013]"



;=======================================
; W0(Complex Matrix) --> Complex Matrix
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=CxMa:"M2,3[0,1,2,3,4,5]"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=CxMa:"M2,3[0,1,2,3,4,5]" RX=CxMa:"M2,3[0,0.567143290409783872999968662210355549754,0.852605502013725491346472414695317466898,1.04990889496403995998869707055289790459,1.20216787319704293921207416549515344750,1.32672466524220022363509929775807966013]"



;=======================================
; W0(Short Integer) --> Error24
;=======================================



;=======================================
; W0(Real) --> Real
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"1"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"1" RX=Real:"0.5671432904097838729999686622103555"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"0"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0" RX=Real:"0"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"-0.36"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"-0.36" RX=Real:"-0.8060843159708177782855213616209920"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"-1"
Out: EC=1 FL_CPXRES=0 FL_ASLIFT=0 RX=Real:"-1"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=1 RX=Real:"-1"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=Real:"-1" RX=Cplx:"-.3181315052047641353126542515876645 i 1.337235701430689408901162143193711"

In:  FL_ASLIFT=0 RX=Real:"0":DEG
Out: EC=24 FL_ASLIFT=0 RX=Real:"0":DEG

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=1 RX=Real:"-0.3678794411714423215955237701614609"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=Real:"-0.3678794411714423215955237701614609" RX=Cplx:"-0.9999999999999999999999999999999999 i 1.330331005430511122188770528833797E-17"


;=======================================
; W0(Complex) --> Complex
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=1 RX=Cplx:"1 i 0"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=Cplx:"1 i 0" RX=Cplx:"0.5671432904097838729999686622103555 i 0"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=1 RX=Cplx:"-0.36 i 0"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=Cplx:"-0.36 i 0" RX=Cplx:"-0.8060843159708177782855213616209920 i 0"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=1 RX=Cplx:"-1 i 0"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=Cplx:"-1 i 0" RX=Cplx:"-.3181315052047641353126542515876645 i 1.337235701430689408901162143193711"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=1 RX=Cplx:"0 i 1"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=Cplx:"0 i 1" RX=Cplx:"0.3746990207371174936059784287597208 i 0.5764127230314352831482892398870685"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=1 RX=Cplx:"0 i -1"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=Cplx:"0 i -1" RX=Cplx:"0.3746990207371174936059784287597208 i -0.5764127230314352831482892398870685"
