// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file mathematics/prime.h
 */
#if !defined(PRIME_H)
  #define PRIME_H

  #include <stdint.h>

  void fnIsPrime      (uint16_t unusedButMandatoryParameter);
  void fnNextPrime    (uint16_t unusedButMandatoryParameter);
  void fnPrimeFactors (uint16_t unusedButMandatoryParameter);

#endif // !PRIME_H
