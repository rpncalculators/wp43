// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file mathematics/percentPlusMG.h
 */
#if !defined(PERCENTPLUSMG_H)
  #define PERCENTPLUSMG_H

  #include <stdint.h>

  void fnPercentPlusMG(uint16_t unusedButMandatoryParameter);

  void percentPlusMGLonILonI(void);
  void percentPlusMGLonIReal(void);
  void percentPlusMGRealLonI(void);
  void percentPlusMGRealReal(void);

#endif // !PERCENTPLUSMG_H
