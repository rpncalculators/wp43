// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file mathematics/ulp.h
 */
#if !defined(ULP_H)
  #define ULP_H

  #include <stdint.h>

  void fnUlp(uint16_t unusedButMandatoryParameter);

#endif // !ULP_H
