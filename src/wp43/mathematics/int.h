// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file mathematics/int.h
 */
#if !defined(INT_H)
  #define INT_H

  #include <stdint.h>

  void fnCheckInteger(uint16_t mode);

#endif // !INT_H
