// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file solver/solver.h
 */
#if !defined(SOLVER_H)
  #define SOLVER_H

  #include "differentiate.h"
  #include "equation.h"
  #include "integrate.h"
  #include "solve.h"
  #include "sumprod.h"
  #include "tvm.h"

#endif // !SOLVER_H
